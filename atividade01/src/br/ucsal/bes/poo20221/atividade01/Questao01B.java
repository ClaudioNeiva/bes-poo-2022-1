package br.ucsal.bes.poo20221.atividade01;

import java.util.Scanner;

// Mudança no arquivo Questao01B.java.
public class Questao01B {

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	private static void obterNotaCalcularExibirConceito2() {
		exibirConceito(calcularConceito(obterNota()));
	}

	private static void exibirConceito(String conceito) {
		System.out.println("Conceito = " + conceito);
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}
		return conceito;
	}

	private static int obterNota() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe uma nota de 0 a 100 (intervalo fechado):");
		return scanner.nextInt();
	}

}
