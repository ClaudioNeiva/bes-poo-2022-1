package br.ucsal.bes.poo20221.atividade01;

import java.util.Scanner;

/* 
 * Suponha que o conceito de um aluno seja determinado em função da sua nota. Suponha, também, 
 * que esta nota seja um valor inteiro na faixa de 0 a 100 (intervalo fechado), conforme a seguinte faixa:

	0 a 49 	= Insuficiente
	50 a 64 = Regular
	65 a 84 = Bom
	85 a 100= Ótimo
	
	Crie um programa em Java que leia a nota de um aluno e apresente o conceito do mesmo.
	Não é necessário tratar valores fora da faixa.

	CTRL + SHIFT + F = formatar o código.
	
	ALT + SHIFT + T = refatorar.
	
	ALT + SHIFT + M = extrair métodos.
	
	CTRL + 1 = dica para solução de um problema qualquer.
	
	ALT + SHIFT + R = renomear.
	
	// Teste de atualização.

 */
public class Questao01 {

	public static void main(String[] args) {
		obterNotaCalcularExibirConceito();
	}

	private static void obterNotaCalcularExibirConceito() {
		int nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		System.out.println("Conceito = " + conceito);
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}
		return conceito;
	}

	private static int obterNota() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe a nota (0 a 100, intervalo fechado):");
		return scanner.nextInt();
	}

}
