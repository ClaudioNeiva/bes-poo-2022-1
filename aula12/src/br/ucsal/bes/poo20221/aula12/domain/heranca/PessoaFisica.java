package br.ucsal.bes.poo20221.aula12.domain.heranca;

//FIXME Os atributos devem ser privados.
public abstract class PessoaFisica extends Pessoa {

	String cpf;
	
	String nomeMae;
	
	String rg;
	
	// TODO Criar subclasses para poder sobrescrever esse m�todo. 
	public abstract String obterIdentificacaoConselhoClasse();
	
}
