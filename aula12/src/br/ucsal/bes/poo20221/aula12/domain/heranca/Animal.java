package br.ucsal.bes.poo20221.aula12.domain.heranca;

import java.time.LocalDate;

//FIXME Os atributos devem ser privados.
public class Animal implements Identificado, Identificado2<Long> {

	String nome;
	
	LocalDate dataNascimento;
	
	String especie;
	
	Pessoa tutor;

	@Override
	public Long getIdentificador2() {
		return 1L;
	}

	@Override
	public String getTipo() {
//		return "O que eu quiser";
		return Identificado2.super.getTipo();
	}

	@Override
	public String getIdentificador() {
		// TODO Auto-generated method stub
		return null;
	}

}
