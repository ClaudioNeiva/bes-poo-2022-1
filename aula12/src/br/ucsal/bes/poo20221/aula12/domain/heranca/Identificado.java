package br.ucsal.bes.poo20221.aula12.domain.heranca;

public interface Identificado {

	String getIdentificador();

	default String getTipo() {
		return "tipo na interface";
	}

}
