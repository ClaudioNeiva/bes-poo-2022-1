package br.ucsal.bes.poo20221.aula12.domain.duasclassesindividuais;

import java.util.List;

//FIXME Os atributos devem ser privados.
public class PessoaJuridica {

	String cnpj;

	String nome;

	String inscricaoEstadual;

	List<String> telefone;

	String logradouro;

	String complemento;

	String bairro;

}
