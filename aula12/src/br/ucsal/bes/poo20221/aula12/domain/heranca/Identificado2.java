package br.ucsal.bes.poo20221.aula12.domain.heranca;

public interface Identificado2<T> {

	T getIdentificador2();
	
	default String getTipo() {
		return "tipo na interface 2";
	}

}
