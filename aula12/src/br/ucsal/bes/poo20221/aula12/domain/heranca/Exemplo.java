package br.ucsal.bes.poo20221.aula12.domain.heranca;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {
		Professor professor1 = new Professor();
		System.out.println(professor1.getTipo());
	}

	void imprimiCarteira(List<Identificado> identificados) {
		for (Identificado identificado : identificados) {
			System.out.println(identificado.getIdentificador() + " | " + identificado.getTipo());
		}
	}

}
