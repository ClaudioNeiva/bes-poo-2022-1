package br.ucsal.bes.poo20221.aula12.domain;

public interface Calculadora<T> {

	T somar(T a, T b);
	
	T subtrair(T a, T b);
	
}
