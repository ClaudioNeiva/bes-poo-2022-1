package br.ucsal.bes.poo20221.atividade02.domain;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

public class Pessoa {

	private static final int QTD_MAX_TELEFONES = 10;

	private String cpf;

	private String nome;

	private Endereco endereco;

	private String telefones[] = new String[QTD_MAX_TELEFONES];

	private List<String> telefonesList = new ArrayList<>();

	private Integer qtdTelefonesCadastrados = 0;

	public Pessoa(String cpf, String nome) {
		this(nome);
		this.cpf = cpf;
		validarCpfRfb(cpf);
	}

	public Pessoa(String cpf, String nome, Endereco endereco, List<String> telefonesList) {
		this(cpf, nome);
		this.endereco = endereco;
		this.telefonesList = telefonesList;
		validarCpfRfb(cpf);
	}

	public Pessoa() {
		// telefones = new String[QTD_MAX_TELEFONES];
		// qtdTelefonesCadastrados = 0;
		// telefonesList = new ArrayList<>();
	}

	public Pessoa(Endereco endereco) {
		this();
		this.endereco = endereco;
	}

	public Pessoa(String nome) {
		this();
		setNome(nome);
	}

	private void validarCpfRfb(String cpf2) {
	}

	public String getCpf() {
		return cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome.toUpperCase();
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefonesList() {
		return new ArrayList<>(telefonesList);
	}

	public String[] getTelefones() {
		String[] telefonesAux = new String[QTD_MAX_TELEFONES];
		for (int i = 0; i < telefonesAux.length; i++) {
			telefonesAux[i] = telefones[i];
		}
		System.out.println("telefones=" + telefones);
		System.out.println("telefonesAux=" + telefonesAux);
		return telefonesAux;
		// return Arrays.copyOf(telefones, QTD_MAX_TELEFONES);
	}

	public void addTelefone(String telefone) {
		if (qtdTelefonesCadastrados == QTD_MAX_TELEFONES) {
			throw new RuntimeException("Qtd máxima de telefones já preenchidos.");
		}
		telefones[qtdTelefonesCadastrados] = telefone;
		qtdTelefonesCadastrados++;
	}

}
