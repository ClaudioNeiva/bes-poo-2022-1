package br.ucsal.bes.poo20221.atividade02.domain;

import java.time.LocalDate;

public class Veiculo {

	private String placa;

	private int anoFabricacao;

	private double valor;

	private Pessoa proprietario;

	public int calcularIdadeVeiculo() {
		return 2022 - anoFabricacao;
//		return LocalDate.now().getYear() - anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		if (anoFabricacao < 1900 || anoFabricacao > LocalDate.now().getYear()) {
			throw new RuntimeException("Valor inválido para o ano de fabricação.");
		}
		this.anoFabricacao = anoFabricacao;
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		if (valor < 0) {
			throw new RuntimeException("Valor não válido. O valor deve ser maior ou igual a zero.");
		}
		this.valor = valor;
	}

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoFabricacao;
		result = prime * result + ((placa == null) ? 0 : placa.hashCode());
		long temp;
		temp = Double.doubleToLongBits(valor);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		if (anoFabricacao != other.anoFabricacao)
			return false;
		if (placa == null) {
			if (other.placa != null)
				return false;
		} else if (!placa.equals(other.placa))
			return false;
		if (Double.doubleToLongBits(valor) != Double.doubleToLongBits(other.valor))
			return false;
		return true;
	}

}
