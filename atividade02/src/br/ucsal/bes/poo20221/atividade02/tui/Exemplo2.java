package br.ucsal.bes.poo20221.atividade02.tui;

import br.ucsal.bes.poo20221.atividade02.domain.Veiculo;

// CTRL + SHIFT + O = organize imports

public class Exemplo2 {

	public static void main(String[] args) {
		Veiculo veiculo1 = new Veiculo();
		veiculo1.setPlaca("ABC-1234");
		veiculo1.setAnoFabricacao(2022);
		veiculo1.setValor(140000d);

		Veiculo veiculo2 = new Veiculo();
		veiculo2.setPlaca("ABC-1234");
		veiculo2.setAnoFabricacao(2022);
		veiculo2.setValor(140000d);

		if(veiculo1 == veiculo2) {
			System.out.println("Apontam para a mesma instância.");
		}else {
			System.out.println("Apontam para instâncias diferentes.");
		}
		
		if(veiculo1.equals(veiculo2)) {
			System.out.println("São veículos iguais.");
		}else {
			System.out.println("São veículos diferentes.");
		}
		
	}

}
