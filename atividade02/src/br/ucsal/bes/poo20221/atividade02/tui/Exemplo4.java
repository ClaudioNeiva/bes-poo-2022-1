package br.ucsal.bes.poo20221.atividade02.tui;

import br.ucsal.bes.poo20221.atividade02.domain.Pessoa;

public class Exemplo4 {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa();
		
		String[] telefonesSimulacao = pessoa1.getTelefones();
		telefonesSimulacao[0] = "5785687";
		
		System.out.println(pessoa1.getTelefones()[0]);
	}
}
