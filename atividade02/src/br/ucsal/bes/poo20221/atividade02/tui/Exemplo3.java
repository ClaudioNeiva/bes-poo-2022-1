package br.ucsal.bes.poo20221.atividade02.tui;

import br.ucsal.bes.poo20221.atividade02.domain.Veiculo;

public class Exemplo3 {

	public static void main(String[] args) {

		Veiculo veiculo1 = new Veiculo();
		Veiculo veiculo2 = new Veiculo();

		veiculo1.setAnoFabricacao(2010);
		veiculo2.setAnoFabricacao(2015);

		System.out.println("veiculo1 -> idade = " + veiculo1.calcularIdadeVeiculo());
		System.out.println("veiculo2 -> idade = " + veiculo2.calcularIdadeVeiculo());

		System.out.println("veiculo1.anoFabricacao= " + veiculo1.getAnoFabricacao());

	}

}
