package br.ucsal.bes.poo20221.atividade02.tui;

import br.ucsal.bes.poo20221.atividade02.domain.Veiculo;

// CTRL + SHIFT + O = organize imports

public class Exemplo1 {

	private static final int QTD_MAX_VEICULOS = 100000000;

	public static void main(String[] args) {

		Veiculo[] veiculos = new Veiculo[QTD_MAX_VEICULOS];

		for (int i = 0; i < QTD_MAX_VEICULOS; i++) {

			veiculos[i] = new Veiculo();
			veiculos[i].setPlaca("ABC-1234");
			veiculos[i].setAnoFabricacao(2022);
			veiculos[i].setValor(140000d);

//			System.out.println(veiculos.toString());
//
//			System.out.println("placa=" + veiculos[i].placa);
//			System.out.println("anoFabricacao=" + veiculos[i].anoFabricacao);
//			System.out.println("valor=" + veiculos[i].valor);

			fazerAlgumaCoisa(veiculos[i]);

			System.out.println("placa=" + veiculos[i].getPlaca());
		}
	}

	public static void fazerAlgumaCoisa(Veiculo veiculo) {
		veiculo.setPlaca("XYZ-1234");
	}

}
