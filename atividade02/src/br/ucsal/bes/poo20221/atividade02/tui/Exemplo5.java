package br.ucsal.bes.poo20221.atividade02.tui;

import br.ucsal.bes.poo20221.atividade02.domain.Endereco;
import br.ucsal.bes.poo20221.atividade02.domain.Pessoa;

public class Exemplo5 {

	public static void main(String[] args) {

		Pessoa pessoa1 = new Pessoa();
		//pessoa1.setCpf("678678678");
		pessoa1.setNome("Claudio Neiva");

		Pessoa pessoa2 = new Pessoa("678678678", "Claudio Neiva");
		pessoa2.setNome("Caju");
		System.out.println("pessoa2.nome=" + pessoa2.getNome());

		Pessoa pessoa3 = new Pessoa(new Endereco());
		Pessoa pessoa4 = new Pessoa("Claudio");
		Pessoa pessoa5 = new Pessoa();
		
		Endereco endereco = new Endereco();
	}

}
