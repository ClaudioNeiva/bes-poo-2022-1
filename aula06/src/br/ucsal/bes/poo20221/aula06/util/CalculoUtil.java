package br.ucsal.bes.poo20221.aula06.util;

public class CalculoUtil {
	
	private CalculoUtil() {
	}

	public static long calcularFatorial(int n) {
		long fat = 1;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
