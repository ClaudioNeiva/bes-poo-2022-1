package br.ucsal.bes.poo20221.aula06.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CalculoUtilParametrizedTest {
	
	@ParameterizedTest
	@CsvSource(value = { "0,1", "1,1", "3,6", "5,120", "6,720" })
	void testarFatorial(int n, long fatorialEsperado) {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
}
