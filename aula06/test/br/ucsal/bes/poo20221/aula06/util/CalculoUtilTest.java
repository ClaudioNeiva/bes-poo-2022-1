package br.ucsal.bes.poo20221.aula06.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtilTest {

	@Test
	void testarFatorial0() {
		int n = 0;
		long fatorialEsperado = 1;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial1() {
		int n = 1;
		long fatorialEsperado = 1;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial3() {
		int n = 3;
		long fatorialEsperado = 6;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial5() {
		int n = 5;
		long fatorialEsperado = 120;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
