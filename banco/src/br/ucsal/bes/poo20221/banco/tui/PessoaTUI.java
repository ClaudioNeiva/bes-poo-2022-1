package br.ucsal.bes.poo20221.banco.tui;

import java.util.List;

import br.ucsal.bes.poo20221.banco.domain.Pessoa;

public class PessoaTUI {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa();

		pessoa1.addTelefone("123456789");
		pessoa1.addTelefone("456745");
		pessoa1.addTelefone("6967867888546");
		pessoa1.addTelefone("345325");

		List<String> telefonesLocal = pessoa1.getTelefones();
		telefonesLocal.add("2");
		pessoa1.getTelefones().add("2");
		System.out.println(telefonesLocal);
		
		System.out.println(pessoa1.getTelefones());
	}

}
