package br.ucsal.bes.poo20221.banco.tui;

import br.ucsal.bes.poo20221.banco.domain.ContaCorrente;
import br.ucsal.bes.poo20221.banco.domain.ContaCorrenteEspecial;
import br.ucsal.bes.poo20221.banco.enums.ErroMessageEnum;
import br.ucsal.bes.poo20221.banco.exception.NegocioException;
import br.ucsal.bes.poo20221.banco.exception.UsuarioOuSenhaInvalidoException;

public class MainTUI {

	public static void main(String[] args) {
		new NegocioException(ErroMessageEnum.USUARIO_OU_SENHA_INVALIDO.getMessage());
		
		new UsuarioOuSenhaInvalidoException();
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//		try {
//			// ContaCorrenteTUI.cadastrar();
//			// exemplificarUso();
//			try {
//				System.out.println(calcularFatorial(-5));
//			} catch (Exception e) {
//				System.out.println(e.getMessage());
//			}
//		} catch (Throwable t) {
//			// escrever o problema em um arquivo de logs.
//			System.out.println("Me comportei mal... prometo melhorar...");
//		}
//	}

	/**
	 * Calcular o fatorial de n.
	 * 
	 * @param n - valor sobre o qual ser� calculado o fatorial, devendo ser maior ou
	 *          igual a zero.
	 * @return fatorial de n.
	 * @throws Exception - exce��o que ocorre qual o n sobre o qual o fatorial ser�
	 *                   calculado n�o � v�lido.
	 */
	public static long calcularFatorial(int n) throws Exception {
		if (n < 0) {
			throw new Exception("N�o � poss�vel calcular fatorial de n�meros negativos.");
		}
		if (n == 0) {
			return 1;
		}
		return n * calcularFatorial(n - 1);
	}

	private static void exemplificarUso() {
		ContaCorrenteEspecial conta1 = new ContaCorrenteEspecial("Claudio Neiva");
		conta1.depositar(1000d);
		conta1.definirLimiteCredito(2000d);
		sacar(conta1, 5000d);
		System.out.println("Terminei o exemplificarUso com sucesso...");
	}

	private static void sacar(ContaCorrente contaCorrente, Double valorSaque) {
		try {
			Double saldoAtual = contaCorrente.sacar(valorSaque);
			System.out.println("Saque realizado com sucesso. Saldo atual " + saldoAtual);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fim do saque, com sucesso ou com falha, o saque foi conclu�do");
	}

}
