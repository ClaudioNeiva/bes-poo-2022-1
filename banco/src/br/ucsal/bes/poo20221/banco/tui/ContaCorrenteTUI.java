package br.ucsal.bes.poo20221.banco.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20221.banco.domain.ContaCorrente;

public class ContaCorrenteTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void cadastrar() {
		System.out.println("*********** CADASTRO CORRENTISTA ***********");
		System.out.println("Informe o nome do correntista:");
		String nomeCorrentista = scanner.nextLine();
		ContaCorrente contaCorrente = new ContaCorrente(nomeCorrentista);
		
		System.out.println(contaCorrente);
	}
}
