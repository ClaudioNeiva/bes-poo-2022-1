package br.ucsal.bes.poo20221.banco.exception;

public class UsuarioOuSenhaInvalidoException extends NegocioException {

	private static final long serialVersionUID = 1L;

	private static final String MESSAGE = "Usu�rio n�o cadastrado ou senha inv�lida.";

	public UsuarioOuSenhaInvalidoException() {
		super(MESSAGE);
	}

}
