package br.ucsal.bes.poo20221.banco.enums;

public enum ErroMessageEnum {
	
	USUARIO_OU_SENHA_INVALIDO("Usu�rio n�o cadastrado ou senha inv�lida."),
	
	CONTA_BLOQUEADA("Conta bloqueada"),
	
	SALDO_INSUFICIENTE("Saldo insuficiente");

	private String message;

	private ErroMessageEnum(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
}
