package br.ucsal.bes.poo20221.banco.domain;

import br.ucsal.bes.poo20221.banco.exception.NegocioException;

public class ContaCorrente {

	// 1 - Atributos (constante, de classe e de inst�ncia)
	// 2 - Construtores
	// 3 - M�todos p�blicos (regra de neg�cio, getter/setters, equals/hascode, etc)
	// 4 - M�todos private (regra de neg�cio, getter/setters, equals/hascode,
	// toString, etc)
	// 5- toString

	private static Integer seq = 0;

	private Integer numero;
	private String nomeCorrentista;
	protected Double saldo;

	public ContaCorrente(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
		saldo = 0d;
		definirNumero();
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	// Sacar -> sucesso (tem dinheiro suficiente na conta para realizar o saque
	// -> falha (N�O tem dinheiro suficiente)
	public Double sacar(Double valor) throws NegocioException {
		validarSaque(valor);
		saldo -= valor;
		return saldo;
	}

	private void validarSaque(Double valor) throws NegocioException {
		if (valor > saldo) {
			throw new NegocioException("Saldo insuficiente.");
		}
	}

	@Override
	public String toString() {
		return "ContaCorrente [numero=" + numero + ", nomeCorrentista=" + nomeCorrentista + ", saldo=" + saldo + "]";
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
	}

	public Integer getNumero() {
		return numero;
	}

	public Double getSaldo() {
		return saldo;
	}

	private void definirNumero() {
		seq++;
		numero = seq;
	}

}
