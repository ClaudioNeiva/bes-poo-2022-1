package br.ucsal.bes.poo20221.banco.domain;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {

	private static final int TAMANHO_MINIMO_TELEFONE = 9;

	private List<String> telefones = new ArrayList<>();

	public List<String> getTelefones() {
		return new ArrayList<>(telefones);
	}

	public void addTelefone(String telefone) {
		if (telefone.length() >= TAMANHO_MINIMO_TELEFONE) {
			telefones.add(telefone);
		}
	}

}
