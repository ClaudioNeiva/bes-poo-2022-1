package br.ucsal.bes.poo20221.banco.domain;

public class ContaCorrenteEspecial extends ContaCorrente {

	private Double limiteCredito;

	public ContaCorrenteEspecial(String nomeCorrentista) {
		super(nomeCorrentista);
		limiteCredito = 0d;
	}

	public void digaQuemVoceEh() {
		System.out.println("Sou um conta corrente especial");
	}

	public void definirLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	// FIXME Utilizar exce��o para indicar a falta de saldo.
	@Override
	public Double sacar(Double valor) {
		if (valor > saldo + limiteCredito) {
			throw new RuntimeException("Saldo insuficiente.");
		}
		saldo -= valor;
		return saldo;
	}

	@Override
	public String toString() {
		return "ContaCorrenteEspecial [limiteCredito=" + limiteCredito + ", toString()=" + super.toString() + "]";
	}

}
