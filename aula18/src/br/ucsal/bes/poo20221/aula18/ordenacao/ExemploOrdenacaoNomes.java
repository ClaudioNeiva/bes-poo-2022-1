package br.ucsal.bes.poo20221.aula18.ordenacao;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ExemploOrdenacaoNomes {

	public static void main(String[] args) {

		List<String> nomes = Arrays.asList("claudio", "antonio", "neiva", "ana", "Maria", "clara");

//		System.out.println("Nomes na ordem que foram informados:");
//		nomes.forEach(System.out::println);
		// for(String nome:nomes) {
		// System.out.println(nome);
		// }

		nomes.sort(String.CASE_INSENSITIVE_ORDER);
		System.out.println("\nNomes na ordem crescente (sem considerar o case):");
		nomes.forEach(System.out::println);

		nomes.sort(Comparator.naturalOrder());
		System.out.println("\nNomes na ordem crescente (considerando o case):");
		nomes.forEach(System.out::println);

//		nomes.sort(Comparator.reverseOrder());
//		System.out.println("\nNomes na ordem decrescente:");
//		nomes.forEach(System.out::println);

	}

}
