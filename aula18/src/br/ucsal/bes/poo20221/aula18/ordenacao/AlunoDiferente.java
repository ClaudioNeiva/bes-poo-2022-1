package br.ucsal.bes.poo20221.aula18.ordenacao;

import java.time.LocalDate;

public class AlunoDiferente extends Aluno {

	public AlunoDiferente(Integer matricula, String nome, LocalDate dataNascimento) {
		super(matricula, nome, dataNascimento);
	}

	@Override
	public int compareTo(Aluno other) {
		return getNome().compareTo(other.getNome());
	}

}
