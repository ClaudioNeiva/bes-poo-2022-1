package br.ucsal.bes.poo20221.aula18.ordenacao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoAlunosDiferente {

	public static void main(String[] args) {

		List<Aluno> alunos = new ArrayList<>();

		alunos.add(new AlunoDiferente(564, "claudio", LocalDate.of(1980, 6, 12)));
		alunos.add(new AlunoDiferente(153, "ana", LocalDate.of(1984, 8, 21)));
		alunos.add(new AlunoDiferente(529, "mara", LocalDate.of(1980, 6, 12)));
		alunos.add(new AlunoDiferente(234, "pedro", LocalDate.of(1984, 8, 21)));
		alunos.add(new AlunoDiferente(275, "clara", LocalDate.of(1980, 6, 12)));

		System.out.println("\nAlunos em ordem crescente 'natural':");
		alunos.sort(Comparator.naturalOrder());
		alunos.forEach(System.out::println);

		// At� o Java 7.
		Collections.sort(alunos, new Comparator<Aluno>() {
			@Override
			public int compare(Aluno o1, Aluno o2) {
				return o1.getMatricula().compareTo(o2.getMatricula());
			}
		});
		System.out.println("\nAlunos em ordem crescente de matricula':");
		alunos.forEach(System.out::println);

		// A partir do Java 8.
		System.out.println("\nAlunos em ordem crescente matricula:");
		alunos.sort(Comparator.comparing(Aluno::getMatricula));
		alunos.forEach(System.out::println);

		// At� o Java 7.
		Collections.sort(alunos, new Comparator<Aluno>() {
			@Override
			public int compare(Aluno o1, Aluno o2) {
				int result = o1.getDataNascimento().compareTo(o2.getDataNascimento());
				if (result == 0) {
					result = o1.getNome().compareTo(o2.getNome());
				}
				return result;
			}
		});
		System.out.println("\nAlunos em ordem crescente de data de nascimento e nome:");
		alunos.forEach(System.out::println);

		// A partir do Java 8.
		System.out.println("\nAlunos em ordem crescente de data de nascimento e nome:");
		alunos.sort(Comparator.comparing(Aluno::getDataNascimento).thenComparing(Aluno::getNome));
		alunos.forEach(System.out::println);
	}
	
}
