package br.ucsal.bes.poo20221.aula18.ordenacao;

import java.time.LocalDate;

public class Aluno implements Comparable<Aluno> {

	private Integer matricula;

	private String nome;

	private LocalDate dataNascimento;

	public Aluno(Integer matricula, String nome, LocalDate dataNascimento) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", dataNascimento=" + dataNascimento + "]";
	}

	@Override
	public int compareTo(Aluno other) {
		return dataNascimento.compareTo(other.dataNascimento);
	}

}
