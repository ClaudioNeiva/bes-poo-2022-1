package br.ucsal.bes.poo20221.aula18.conjunto;

import java.time.LocalDate;

public class AlunosEqualsExemplo {

	public static void main(String[] args) {
		Aluno aluno1 = new Aluno(123, "claudio", LocalDate.of(1973, 10, 20));
		Aluno aluno2 = new Aluno(123, "claudio", LocalDate.of(1973, 10, 20));
		System.out.println(aluno1.equals(aluno2));
		System.out.println(aluno1==aluno2);
	}
	
}
