package br.ucsal.bes.poo20221.aula18.conjunto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class AlunosSetExemplo {

	public static void main(String[] args) {
		Aluno aluno1 = new Aluno(123, "claudio", LocalDate.of(1973, 10, 20));
		Aluno aluno2 = new Aluno(234, "neiva", LocalDate.of(1973, 11, 20));
		System.out.println("aluno1.hashCode()=" + aluno1.hashCode());
		System.out.println("aluno2.hashCode()=" + aluno2.hashCode());
		Set<Aluno> alunos = new HashSet<>();
		alunos.add(aluno1);
		alunos.add(aluno2);
		System.out.println("alunos.contains(aluno1)=" + alunos.contains(aluno1));
		// alunos.remove(aluno1);
		// aluno1.setNome("pedro");
		// alunos.add(aluno1);
		System.out.println("aluno1.hashCode()=" + aluno1.hashCode());
		System.out.println("alunos.contains(aluno1)=" + alunos.contains(aluno1));
	}

}
