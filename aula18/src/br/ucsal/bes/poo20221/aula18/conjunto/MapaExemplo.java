package br.ucsal.bes.poo20221.aula18.conjunto;

import java.util.HashMap;
import java.util.Map;

public class MapaExemplo {

	/*
	 * Criar um programa que solicite 10 n�meros inteiros e retorne os distintos
	 * n�meros e quantas vezes cada um dos distintos n�meros foram informados.
	 * 
	 * Exemplo:
	 * 
	 * Entrada: 4 6 7 4 2 7 7 1 9 5
	 * 
	 * Sa�da esperada:
	 * 
	 * 4 x 2
	 * 
	 * 6 x 1
	 * 
	 * 7 x 3
	 * 
	 * 2 x 1
	 * 
	 * 1 x 1
	 * 
	 * 9 x 1
	 * 
	 * 5 x 1
	 * 
	 */

	public static void main(String[] args) {
		obterNumerosInfomarQtdCadaUmFoiInformadoSemMapa();
		obterNumerosInfomarQtdCadaUmFoiInformadoComMapa();
	}

	private static void obterNumerosInfomarQtdCadaUmFoiInformadoComMapa() {
		// Map < key, value>
		// Map < n�mero, qtd-vezes-n�mero-foi-informado>
		Map<Integer, Integer> numerosQtdRepeticoes = new HashMap<>();
	}

	private static void obterNumerosInfomarQtdCadaUmFoiInformadoSemMapa() {
		Integer[] numerosDistintos;
		Integer[] qtdRepeticoesNumero;
	}
}
