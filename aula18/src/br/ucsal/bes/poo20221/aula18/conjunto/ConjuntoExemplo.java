package br.ucsal.bes.poo20221.aula18.conjunto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class ConjuntoExemplo {

	private static final int QTD_NOMES = 5;
	private static Scanner scanner = new Scanner(System.in);

	// Obter 5 nomes distintos e listar os nomes informados.

	public static void main(String[] args) {
		// obterListarNomesUsandoList();
		obterListarNomesUsandoSet();
	}

	private static void obterListarNomesUsandoSet() {
		Set<String> nomesDistintos = new HashSet<>();
		// Set<String> nomesDistintos = new LinkedHashSet<>();
		// Set<String> nomesDistintos = new TreeSet<>();
		System.out.println("Informe " + QTD_NOMES + " distintos:");
		do {
			String nomeInformado = scanner.nextLine();
			nomesDistintos.add(nomeInformado);
		} while (nomesDistintos.size() < QTD_NOMES);
		System.out.println("Nomes distintos informados:");
		nomesDistintos.forEach(System.out::println);
	}

	private static void obterListarNomesUsandoList() {
		List<String> nomesDistintos = new ArrayList<>();
		System.out.println("Informe " + QTD_NOMES + " distintos:");
		do {
			String nomeInformado = scanner.nextLine();
			if (!nomesDistintos.contains(nomeInformado)) {
				nomesDistintos.add(nomeInformado);
			}
		} while (nomesDistintos.size() < QTD_NOMES);
		System.out.println("Nomes distintos informados:");
		nomesDistintos.forEach(System.out::println);
	}

}
