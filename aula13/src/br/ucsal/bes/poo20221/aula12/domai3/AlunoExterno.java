package br.ucsal.bes.poo20221.aula12.domai3;

public class AlunoExterno extends Aluno {

	private String nomeEscolaOrigem;

	public AlunoExterno(String nome) {
		super(nome);
	}

	public String getNomeEscolaOrigem() {
		return nomeEscolaOrigem;
	}

	public void setNomeEscolaOrigem(String nomeEscolaOrigem) {
		this.nomeEscolaOrigem = nomeEscolaOrigem;
	}

	public static Integer getSeq() {
		return 1000;
	}

	private void metodo1() {
		System.out.println("m�todo1 = manga");
	}

	@Override
	public void metodo2() {
		System.out.println("m�todo2 = melancia");
	}

}
