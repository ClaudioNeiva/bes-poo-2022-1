package br.ucsal.bes.poo20221.aula12.domai3;

public class Aluno {

	private static Integer seq = 0;

	private Integer matricula;

	private String nome;

	public Aluno(String nome) {
		super();
		seq++;
		this.matricula = seq;
		this.nome = nome;
		metodo1(); // compila��o -> Aluno.linha21
		metodo2(); // execu��o -> n�o sei agora o que vai rodar
	}

	private void metodo1() {
		System.out.println("m�todo1 = jaca");
	}
	
	protected void metodo2() {
		System.out.println("m�todo2 = goiaba");
	}
	
	public static Integer getSeq() {
		return seq;
	}
	
	public Integer getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + "]";
	}

}
