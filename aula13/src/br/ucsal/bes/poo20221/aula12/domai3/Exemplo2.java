package br.ucsal.bes.poo20221.aula12.domai3;

public class Exemplo2 {

	public static void main(String[] args) {
		AlunoExterno aluno1 = new AlunoExterno("pedro");
		Aluno aluno2 = new Aluno("pedro");
		exibir(aluno1);
		exibir(aluno2);
	}
	
	public static void exibir(Aluno aluno) {
		System.out.println(aluno.getNome());
	}

}
