package br.ucsal.bes.poo20221.aula12.domai3;

public class Exemplo1 {

	public static void main(String[] args) {

		Aluno alunoA = new Aluno("Claudio");
		Aluno alunoB = new Aluno("Maria");

		System.out.println(alunoA.getMatricula());
		System.out.println(alunoB.getMatricula());

		Aluno alunoC = null;
		// NUNCA chame m�todos est�ticos a partir de vari�veis, pois a execu��o ser� a
		// partir da classe da vari�vel. Escrevendo a partir da vari�vel, voc� provocar�
		// confus�o em quem l� seu c�digo.
		System.out.println(alunoC.getSeq());
		
		// Escreva sempre assim:
		System.out.println(Aluno.getSeq());
		
		new AlunoExterno("pedro");
	}

}
