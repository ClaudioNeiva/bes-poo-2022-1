package br.ucsal.bes.poo20221.aula17;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoAlunos {

	public static void main(String[] args) {

		List<Aluno> alunos = new ArrayList<>();

		alunos.add(new Aluno(564, "claudio", LocalDate.of(1980, 6, 12)));
		alunos.add(new Aluno(153, "ana", LocalDate.of(1984, 8, 21)));
		alunos.add(new Aluno(529, "mara", LocalDate.of(1980, 6, 12)));
		alunos.add(new Aluno(234, "pedro", LocalDate.of(1984, 8, 21)));
		alunos.add(new Aluno(275, "clara", LocalDate.of(1980, 6, 12)));

		System.out.println("Alunos na ordem que foram cadastrados:");
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem crescente 'natural':");
		alunos.sort(Comparator.naturalOrder());
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem crescente nome:");
		alunos.sort(Comparator.comparing(Aluno::getNome));
		alunos.forEach(System.out::println);

		System.out.println("\nAlunos em ordem crescente data e nome:");
		alunos.sort(Comparator.comparing(Aluno::getDataNascimento).thenComparing(Aluno::getNome));
		alunos.forEach(System.out::println);

	}

}
