package br.ucsal.bes.poo20221.aula17;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ExemploOrdenacaoNomes {

	public static void main(String[] args) {

		List<String> nomes = Arrays.asList("claudio", "antonio", "neiva", "ana", "maria", "clara");

		System.out.println("Nomes na ordem que foram informados:");
		nomes.forEach(System.out::println);
		// for(String nome:nomes) {
		// System.out.println(nome);
		// }

		nomes.sort(Comparator.naturalOrder());
		System.out.println("\nNomes na ordem crescente:");
		nomes.forEach(System.out::println);

		nomes.sort(Comparator.reverseOrder());
		System.out.println("\nNomes na ordem decrescente:");
		nomes.forEach(System.out::println);

	}

}
