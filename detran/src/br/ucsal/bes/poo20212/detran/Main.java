package br.ucsal.bes.poo20212.detran;

import br.ucsal.bes.poo20212.detran.gui.LocadoraGUI;
import br.ucsal.bes.poo20212.detran.tui.MenuTUI;

public class Main {

	public static void main(String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("tui")) {
			MenuTUI.executar();
		} else {
			LocadoraGUI.inicializar();
		}
	}

}
