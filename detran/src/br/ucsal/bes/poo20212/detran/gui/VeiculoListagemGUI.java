package br.ucsal.bes.poo20212.detran.gui;

import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.ucsal.bes.poo20212.detran.domain.Veiculo;
import br.ucsal.bes.poo20212.detran.persistence.VeiculoDAO;

public class VeiculoListagemGUI extends JPanel {

	private static final long serialVersionUID = 1L;

	private JTable table;
	private DefaultTableModel model;

	/**
	 * Create the panel.
	 */
	public VeiculoListagemGUI() {
		String[] colunas = { "Placa", "Ano de fabricação", "Valor" };
		model = new DefaultTableModel(null, colunas);
		table = new JTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
		listarVeiculos();
	}

	private void listarVeiculos() {
		
		List<Veiculo> veiculos = VeiculoDAO.findAll();
		
		for (Veiculo veiculo : veiculos) {
			model.addRow(new Object[] { veiculo.getPlaca(), veiculo.getAnoFabricacao(), veiculo.getValor() });

			// Object[] veiculoRow = new Object[3];
			// veiculoRow[0] = veiculo.getPlaca();
			// veiculoRow[1] = veiculo.getAnoFabricacao();
			// veiculoRow[2] = veiculo.getValor();
			// model.addRow(veiculoRow);

			// Object[] veiculoRow = new Object[] { veiculo.getPlaca(),
			// veiculo.getAnoFabricacao(), veiculo.getValor() };
			// model.addRow(veiculoRow);

		}
	}

}
