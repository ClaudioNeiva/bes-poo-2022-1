package br.ucsal.bes.poo20212.detran.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ucsal.bes.poo20212.detran.business.VeiculoBO;
import br.ucsal.bes.poo20212.detran.domain.Veiculo;
import br.ucsal.bes.poo20212.detran.exception.NegocioException;

public class VeiculoCadastroGUI extends JPanel {

	private static final long serialVersionUID = 1L;

	private JTextField tfPlaca;
	private JTextField tfAnoFabricacao;
	private JTextField tfValor;

	/**
	 * Create the panel.
	 */
	public VeiculoCadastroGUI() {
		setLayout(null);
		addCampoPlaca();
		addCampoAnoFabricacao();
		addCampoValor();
		addBotaoConfirmar();
	}

	private void addBotaoConfirmar() {
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cadastrar();
			}
		});
		btnConfirmar.setBounds(12, 215, 117, 25);
		add(btnConfirmar);
	}

	private void addCampoValor() {
		JLabel lblValor = new JLabel("Valor:");
		lblValor.setBounds(12, 151, 70, 15);
		add(lblValor);

		tfValor = new JTextField();
		tfValor.setBounds(177, 149, 114, 19);
		add(tfValor);
		tfValor.setColumns(10);
	}

	private void addCampoAnoFabricacao() {
		JLabel lblAnoDeFabricao = new JLabel("Ano de fabricação:");
		lblAnoDeFabricao.setBounds(12, 108, 144, 15);
		add(lblAnoDeFabricao);

		tfAnoFabricacao = new JTextField();
		tfAnoFabricacao.setBounds(177, 106, 114, 19);
		add(tfAnoFabricacao);
		tfAnoFabricacao.setColumns(10);
	}

	private void addCampoPlaca() {
		JLabel lblPlaca = new JLabel("Placa:");
		lblPlaca.setBounds(12, 69, 70, 15);
		add(lblPlaca);

		tfPlaca = new JTextField();
		tfPlaca.setBounds(177, 67, 114, 19);
		add(tfPlaca);
		tfPlaca.setColumns(10);
	}

	protected void cadastrar() {

		String placa = tfPlaca.getText();
		Integer anoFabricacao = Integer.parseInt(tfAnoFabricacao.getText());
		Double valor = Double.parseDouble(tfValor.getText());
		
		Veiculo veiculo = new Veiculo(placa, anoFabricacao, valor);

		try {
			VeiculoBO.incluir(veiculo);
			JOptionPane.showMessageDialog(this, "Inclusão feita com sucesso.");
			tfPlaca.setText("");
			tfAnoFabricacao.setText("");
			tfValor.setText("");
		} catch (NegocioException e) {
			JOptionPane.showMessageDialog(this, e.getMessage());
		}
	}
}
