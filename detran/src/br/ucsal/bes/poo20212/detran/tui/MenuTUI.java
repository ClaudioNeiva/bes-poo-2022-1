package br.ucsal.bes.poo20212.detran.tui;

import br.ucsal.bes.poo20212.detran.tui.util.UtilTUI;

public class MenuTUI {

	public static void executar() {
		OpcaoMenuEnum opcaoSelecionada = null;
		do {
			try {
				exibirOpcoes();
				opcaoSelecionada = obterOpcaoSelecionada();
				executarOpcao(opcaoSelecionada);
			} catch (Exception e) {
				System.out.println("Algo de errado. Entre em contato com o suporte, informando erro 0134. (Erro="
						+ e.getClass() + ")");
			}
		} while (opcaoSelecionada == null || !opcaoSelecionada.equals(OpcaoMenuEnum.SAIR));
	}

	private static void executarOpcao(OpcaoMenuEnum opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case INCLUIR_PROPRIETARIO:
			System.out.println("Aqui vou chamar o método para incluir um proprietário...");
			break;
		case INCLUIR_VEICULO:
			VeiculoTUI.incluir();
			break;
		case ALTERAR_VEICULO:
			VeiculoTUI.alterar();
			break;
		case LISTAR_VEICULOS:
			VeiculoTUI.listar();
			break;
		case CALCULAR_IMPOSTO:
			VeiculoTUI.calcularImposto();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		default:
			System.out.println("Opção não válida.");
			break;
		}
	}

	private static OpcaoMenuEnum obterOpcaoSelecionada() {
		Integer opcaoInt = UtilTUI.obterInteger("Informe a opção desejada:");
		return OpcaoMenuEnum.valueOfInteger(opcaoInt);
	}

	private static void exibirOpcoes() {
		System.out.println("\n\n############ MENU ############");
		for (OpcaoMenuEnum opcaoMenu : OpcaoMenuEnum.values()) {
			System.out.println(opcaoMenu.obterDescricaoCompleta());
		}
	}

}
