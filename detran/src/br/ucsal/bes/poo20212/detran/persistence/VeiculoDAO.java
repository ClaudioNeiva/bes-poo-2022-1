package br.ucsal.bes.poo20212.detran.persistence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20212.detran.domain.Veiculo;
import br.ucsal.bes.poo20212.detran.exception.NegocioException;
import br.ucsal.bes.poo20212.detran.util.FileUtil;

public class VeiculoDAO {

	private static final String NOME_ARQUIVO = "./veiculos.dat";

	private static List<Veiculo> veiculos;

	static {
		carregarVeiculosDoArquivo();
	}

	private VeiculoDAO() {
	}

	public static void insert(Veiculo veiculo) throws NegocioException {
		veiculos.add(veiculo);
		FileUtil.gravar(NOME_ARQUIVO, veiculos);
	}

	public static void update(Veiculo veiculo) throws NegocioException {
		// Não é necessário atualizar o veículo, pois o ponteiro aponta para o mesmo
		// objeto que já está no List. Caso você venha a trablhar com banco de dados,
		// aqui você faria um update no banco.
		FileUtil.gravar(NOME_ARQUIVO, veiculos);
	}

	public static void remove(int pos) throws NegocioException {
		veiculos.remove(pos);
		FileUtil.gravar(NOME_ARQUIVO, veiculos);
	}

	public static Veiculo findByIndex(int pos) {
		return veiculos.get(pos);
	}

	public static Veiculo findByPlaca(String placa) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equals(placa)) {
				return veiculo;
			}
		}
		return null;
	}

	public static List<Veiculo> findAll() {
		return new ArrayList<>(veiculos);
	}

	public static Veiculo obterPorPlaca(String placa) throws NegocioException {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(placa)) {
				return veiculo;
			}
		}
		throw new NegocioException("Veículo não encontrado.");
	}

	@SuppressWarnings("unchecked")
	private static void carregarVeiculosDoArquivo() {
		try {
			veiculos = (ArrayList<Veiculo>) FileUtil.ler(NOME_ARQUIVO);
		} catch (ClassNotFoundException | IOException e) {
			veiculos = new ArrayList<>();
		}
	}

}
