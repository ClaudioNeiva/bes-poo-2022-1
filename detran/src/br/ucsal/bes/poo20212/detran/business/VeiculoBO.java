package br.ucsal.bes.poo20212.detran.business;

import java.time.LocalDate;

import br.ucsal.bes.poo20212.detran.domain.Veiculo;
import br.ucsal.bes.poo20212.detran.exception.NegocioException;
import br.ucsal.bes.poo20212.detran.persistence.VeiculoDAO;

// CTRL + SHIFT + G

public class VeiculoBO {

	private VeiculoBO() {
	}

	public static void incluir(Veiculo veiculo) throws NegocioException {
		validar(veiculo);
		VeiculoDAO.insert(veiculo);
	}

	public static void alterar(Veiculo veiculo, String placa, int anoFabricacao, double valor) throws NegocioException {
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setValor(valor);
		validar(veiculo);
		VeiculoDAO.update(veiculo);
	}

	/**
	 * Método que calcula o imposto do veículo cuja placa é passada como parâmetro.
	 * 
	 * @param placa do veículo sobre o qual será calculado o imposto
	 * @return valor do imposto cálculado para o veículo passado como parâmetro
	 * @throws NegocioException para veículos não encontrados e para veículos sem
	 *                          valor ou ano de fabricação
	 */
	public static Double calcularImposto(String placa) throws NegocioException {
		Veiculo veiculo = VeiculoDAO.obterPorPlaca(placa);
		if (veiculo.getValor() == null) {
			System.out.println();
			throw new NegocioException("ERRO: Valor do veículo não definido. Não é possível calcular o imposto.");
		}
		if (veiculo.getAnoFabricacao() == null) {
			throw new NegocioException(
					"ERRO: Ano de fabricação do veículo não definido. Não é possível calcular o imposto.");
		}
		return veiculo.getValor() * .01 * veiculo.getAnoFabricacao();
	}

	private static void validar(Veiculo veiculo) throws NegocioException {
		validarPlaca(veiculo);
		validarAnoFabricacao(veiculo);
		validarValor(veiculo);
	}

	private static void validarValor(Veiculo veiculo) throws NegocioException {
		if (veiculo.getValor() < 0) {
			throw new NegocioException("ERRO: O valor do veículo deve ser maior que zero.");
		}
	}

	private static void validarAnoFabricacao(Veiculo veiculo) throws NegocioException {
		int anoAtual = LocalDate.now().getYear();

		if (veiculo.getAnoFabricacao() > anoAtual) {
			throw new NegocioException("ERRO: O ano de fabricação não pode ser maior que o ano atual.");
		}
	}

	private static void validarPlaca(Veiculo veiculo) throws NegocioException {
		if (veiculo.getPlaca().trim().isEmpty()) {
			throw new NegocioException("ERRO: A placa do veículo deve ser informada.");
		}
	}

}
