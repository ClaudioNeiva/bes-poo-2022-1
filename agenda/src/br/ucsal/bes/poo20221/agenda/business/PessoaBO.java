package br.ucsal.bes.poo20221.agenda.business;

import br.ucsal.bes.poo20221.agenda.domain.Pessoa;
import br.ucsal.bes.poo20221.agenda.persistence.PessoaDAO;

public class PessoaBO {

	public static void cadastrar(Pessoa pessoa) {
		validar(pessoa);
		PessoaDAO.persistir(pessoa);
	}

	private static void validar(Pessoa pessoa) {
		if (pessoa.getNome() == null || pessoa.getNome().isBlank()) {
			throw new RuntimeException("Nome n�o informado.");
		}
		if (pessoa.getTelefone() == null || pessoa.getTelefone().isBlank()) {
			throw new RuntimeException("Telefone n�o informado.");
		}
	}

}
