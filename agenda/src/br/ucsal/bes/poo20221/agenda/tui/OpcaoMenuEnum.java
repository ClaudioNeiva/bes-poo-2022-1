package br.ucsal.bes.poo20221.agenda.tui;

//CTRL + SHIFT + T = Types (classes, enums, etc)

//CTRL + SHIFT + R = Resources (qualquer arquivo dentro do projeto, inclusive .java, .txt, etc)

public enum OpcaoMenuEnum {

	CADASTRAR(1, "Cadastrar"),

	PESQUISAR(2, "Pesquisar"),

	EXCLUIR(3, "Excluir"),

	LISTAR(4, "Listar"),

	SAIR(9, "Sair");

	private Integer codigo;
	private String descricao;

	private OpcaoMenuEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescricaoCompleta() {
		return codigo + " - " + descricao;
	}

	public static OpcaoMenuEnum valueOfInt(Integer codigo) {
		for (OpcaoMenuEnum opcaoMenu : values()) {
			if (opcaoMenu.codigo.equals(codigo)) {
				return opcaoMenu;
			}
		}
		throw new IllegalArgumentException(
				"N�o existe br.ucsal.bes.poo20221.agenda.tui.OpcaoMenuEnum.codigo(" + codigo + ")");
	}

}
