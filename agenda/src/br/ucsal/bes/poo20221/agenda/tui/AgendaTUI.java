package br.ucsal.bes.poo20221.agenda.tui;

public class AgendaTUI {

	private AgendaTUI() {
	}

	public static void main(String[] args) {
		MenuTUI.executar();
	}

}
