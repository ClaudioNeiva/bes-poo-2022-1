package br.ucsal.bes.poo20221.agenda.tui;

public class MenuTUI {
	private MenuTUI() {
	}

	public static void executar() {
		OpcaoMenuEnum opcaoSelecionada;
		do {
			exibirOpcoes();
			opcaoSelecionada = obterOpcaoSelecionada();
			executar(opcaoSelecionada);
		} while (!OpcaoMenuEnum.SAIR.equals(opcaoSelecionada));
	}

	private static void exibirOpcoes() {
		System.out.println("********** AGENDA **********");
		for (OpcaoMenuEnum opcao : OpcaoMenuEnum.values()) {
			System.out.println(opcao.getDescricaoCompleta());
		}
	}

	private static OpcaoMenuEnum obterOpcaoSelecionada() {
		int opcaoSelecionadaInt = TuiUtil.obterInteger("Informe a op��o desejada:");
		return OpcaoMenuEnum.valueOfInt(opcaoSelecionadaInt);
		// String opcaoSelecionadaString = scanner.nextLine();
		// return OpcaoMenuEnum.valueOf(opcaoSelecionadaString.toUpperCase());
	}

	private static void executar(OpcaoMenuEnum opcaoSelecionada) {
		switch (opcaoSelecionada) {
		case CADASTRAR:
			PessoaTUI.cadastrar();
			break;
		case PESQUISAR:
			PessoaTUI.pesquisar();
			break;
		case EXCLUIR:
			PessoaTUI.excluir();
			break;
		case LISTAR:
			PessoaTUI.listar();
			break;
		default:
			System.out.println("Bye...");
			break;
		}
	}

}
