package br.ucsal.bes.poo20221.agenda.tui;

import java.util.List;

import br.ucsal.bes.poo20221.agenda.business.PessoaBO;
import br.ucsal.bes.poo20221.agenda.domain.Pessoa;
import br.ucsal.bes.poo20221.agenda.persistence.PessoaDAO;

public class PessoaTUI {

	private PessoaTUI() {
	}

	public static void cadastrar() {
		System.out.println("********** CADASTRO DE PESSOAS **********");
		String nome = TuiUtil.obterString("Informe o nome:");
		String telefone = TuiUtil.obterString("Informe o telefone:");
		String endereco = TuiUtil.obterString("Informe o endere�o:");
		Pessoa pessoa = new Pessoa(nome, telefone, endereco);
		PessoaBO.cadastrar(pessoa);
	}

	public static void pesquisar() {
		System.out.println("********** PESQUISA DE PESSOAS **********");
		System.out.println("Op��o n�o implementada...");
	}

	public static void excluir() {
		System.out.println("********** EXCLUS�O DE PESSOAS **********");
		System.out.println("Op��o n�o implementada...");
	}

	public static void listar() {
		System.out.println("********** LISTAGEM DE PESSOAS **********");
		List<Pessoa> pessoas = PessoaDAO.obterTodas();
		for (Pessoa pessoa : pessoas) {
			System.out.print("\nNome: " + pessoa.getNome());
			System.out.print("\tTelefone: " + pessoa.getTelefone());
			System.out.println("\tEndere�o: " + pessoa.getEndereco());
		}
	}

}
