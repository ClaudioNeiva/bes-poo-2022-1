package br.ucsal.bes.poo20221.agenda.tui;

import java.util.Scanner;

public class TuiUtil {

	private static Scanner scanner = new Scanner(System.in);

	private TuiUtil() {
	}

	public static Integer obterInteger(String mensagem) {
		System.out.println(mensagem);
		int valor = scanner.nextInt();
		scanner.nextLine();
		return valor;
	}

	public static Double obterDouble(String mensagem) {
		System.out.println(mensagem);
		Double valor = scanner.nextDouble();
		scanner.nextLine();
		return valor;
	}

	public static String obterString(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

}
